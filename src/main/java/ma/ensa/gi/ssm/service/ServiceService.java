package ma.ensa.gi.ssm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import ma.ensa.gi.ssm.dao.ServiceDao;
import ma.ensa.gi.ssm.model.Entityservice;

@Service
public class ServiceService {

	@Autowired
	ServiceDao servicedao;
	
	public Entityservice create(Entityservice service) {
		return servicedao.save(service);
    }
}
