package ma.ensa.gi.ssm.model;

import javax.persistence.*;

import lombok.Data;

import java.util.Date;

@Entity
@Data
public class Commentaire {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Long id_commentaire;
	@Temporal(TemporalType.DATE)
	Date time;
	String etat;
	String message;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tache")
	Tache tache;

	public Commentaire() {
	}

	public Commentaire(String etat, String message, Tache tache) {
		super();
		this.etat = etat;
		this.message = message;
		this.tache = tache;
	}

	public Long getId_commentaire() {
		return id_commentaire;
	}

	public void setId_commentaire(Long id_commentaire) {
		this.id_commentaire = id_commentaire;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Tache getTache() {
		return tache;
	}

	public void setTache(Tache tache) {
		this.tache = tache;
	}
}
