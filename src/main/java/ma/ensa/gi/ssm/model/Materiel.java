package ma.ensa.gi.ssm.model;


import java.util.Date;

import javax.persistence.*;

@Entity
public class Materiel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Long id_materiel;
	String nom_materiel;
	@Temporal(TemporalType.DATE)
	Date date_reception;
	Float cout;
	
	/*@ManyToMany
	@JoinTable(name="materiel_proejt")
	@joinColumn(name="id_materiel")*/
	
}
