package ma.ensa.gi.ssm.model;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

@Entity
@Table(name="projet")
public class Projet implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Long id_projet;
	String objectif;
	@Temporal(TemporalType.DATE)
	Date datedebut;
	@Temporal(TemporalType.DATE)
	Date datefin;
	Double budget;
	String etat;
	
	@ManyToOne //(fetch = FetchType.LAZY)
	@JoinColumn(name="id_service")
	Entityservice serviceE;

	@OneToMany(mappedBy = "projet", cascade = CascadeType.ALL, orphanRemoval = true)
	List<Tache> taches = new ArrayList<Tache>();

	public Projet() {
		super();
	}

	public Projet(String objectif, Date datedebut, Date datefin, Double budget, String etat, Entityservice serviceE, List<Tache> taches) {
		this.objectif = objectif;
		this.datedebut = datedebut;
		this.datefin = datefin;
		this.budget = budget;
		this.etat = etat;
		this.serviceE = serviceE;
		this.taches = taches;
	}

	public Date getDatedebut() {
		return datedebut;
	}

	public void setDatedebut(Date datedebut) {
		this.datedebut = datedebut;
	}

	public Date getDatefin() {
		return datefin;
	}

	public void setDatefin(Date datefin) {
		this.datefin = datefin;
	}

	public Long getId_projet() {
		return id_projet;
	}

	public void setId_projet(Long id_projet) {
		this.id_projet = id_projet;
	}

	public String getObjectif() {
		return objectif;
	}

	public void setObjectif(String objectif) {
		this.objectif = objectif;
	}

	public Double getBudget() {
		return budget;
	}

	public void setBudget(Double budget) {
		this.budget = budget;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public Entityservice getServiceE() {
		return serviceE;
	}

	public void setServiceE(Entityservice serviceE) {
		this.serviceE = serviceE;
	}

	/*public List<Tache> getTaches() {
		return taches;
	}*/

	public void setTaches(List<Tache> taches) {
		this.taches = taches;
	}
}
