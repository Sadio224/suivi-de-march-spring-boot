package ma.ensa.gi.ssm.model;



import javax.persistence.*;

import lombok.Data;

import java.io.Serializable;


@Entity
@Data
public class Personnel implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Long id_personnel;
	
	String nom;
	String prenom;
	String profession;

	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name="id_service")
	Entityservice service;

	public Personnel() {
		super();
	}

	public Personnel(String nom, String prenom, String profession, Entityservice service) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.profession = profession;
		this.service = service;
	}

	public Long getId_personnel() {
		return id_personnel;
	}

	public void setId_personnel(Long id_personnel) {
		this.id_personnel = id_personnel;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public Entityservice getService() {
		return service;
	}

	public void setService(Entityservice service) {
		this.service = service;
	}
	
	
}
