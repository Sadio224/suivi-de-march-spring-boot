package ma.ensa.gi.ssm.model;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import lombok.*;

@Entity
public class Tache implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Long id_tache;
	String intitule;
	@Temporal(TemporalType.DATE)
	Date datedebut;
	@Temporal(TemporalType.DATE)
	Date datefin;
	String duree;
	String etat;

	/*@OneToMany(targetEntity=Commentaire.class,mappedBy = "tache", cascade = CascadeType.ALL, orphanRemoval = true)
	List<Commentaire> comments = new LinkedList();*/
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_projet")
	Projet projet;

	public Tache() {
	}

	public Tache(String intitule, Date datedebut, Date datefin, String duree, String etat, Projet projet) {
		this.intitule = intitule;
		this.datedebut = datedebut;
		this.datefin = datefin;
		this.duree = duree;
		this.etat = etat;
		this.projet = projet;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public Date getDatedebut() {
		return datedebut;
	}

	public void setDatedebut(Date datedebut) {
		this.datedebut = datedebut;
	}

	public Date getDatefin() {
		return datefin;
	}

	public void setDatefin(Date datefin) {
		this.datefin = datefin;
	}

	public Long getId_tache() {
		return id_tache;
	}

	public void setId_tache(Long id_tache) {
		this.id_tache = id_tache;
	}

	public String getDuree() {
		return duree;
	}

	public void setDuree(String duree) {
		this.duree = duree;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	/*public List<Commentaire> getComments() {
		return comments;
	}

	public void setComments(List<Commentaire> comments) {
		this.comments = comments;
	}*/

	public Projet getProjet() {
		return projet;
	}

	public void setProjet(Projet projet) {
		this.projet = projet;
	}
}
