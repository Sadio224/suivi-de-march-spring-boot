package ma.ensa.gi.ssm.model;


import java.io.Serializable;
import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="service")
public class Entityservice implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Long id_service;
	String name_service;
	
	@OneToMany(mappedBy = "serviceE", cascade = CascadeType.ALL, orphanRemoval = true)
	List<Projet> projets;
	
	@OneToMany(mappedBy = "service", cascade = CascadeType.ALL, orphanRemoval = true)
	List<Personnel> personnels;
	
	public Entityservice() {
		super();
	}
	

	public Entityservice(Long id_service, String name_service, List<Projet> projets, List<Personnel> personnels) {
		super();
		this.id_service = id_service;
		this.name_service = name_service;
		this.projets = projets;
		this.personnels = personnels;
	}



	public Long getId_service() {
		return id_service;
	}

	public void setId_service(Long id_service) {
		this.id_service = id_service;
	}

	public String getName_service() {
		return name_service;
	}

	public void setName_service(String name_service) {
		this.name_service = name_service;
	}

	/*public List<Projet> getProjet() {
		return projets;
	}*/

	public void setProjet(List<Projet> projets) {
		this.projets = projets;
	}

	/*public List<Personnel> getPersonnels() {
		return personnels;
	}

	public void setPersonnels(List<Personnel> personnels) {
		this.personnels = personnels;
	}*/

	
	
	//////////////////////////////////////////////////////
	
	
}
