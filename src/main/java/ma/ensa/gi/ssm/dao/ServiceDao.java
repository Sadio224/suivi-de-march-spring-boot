package ma.ensa.gi.ssm.dao;

//import java.security.Provider.Service;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.ensa.gi.ssm.model.Entityservice;

public interface ServiceDao extends JpaRepository<Entityservice, Long> {
	
	
}
