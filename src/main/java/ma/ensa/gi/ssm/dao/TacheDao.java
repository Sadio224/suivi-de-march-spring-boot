package ma.ensa.gi.ssm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.ensa.gi.ssm.model.Tache;

public interface TacheDao extends JpaRepository<Tache, Long> {

}
