package ma.ensa.gi.ssm.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import ma.ensa.gi.ssm.model.Projet;

public interface ProjetDao extends JpaRepository<Projet, Long> {
	
	/*@Query("select p from Projet p where p.getService() like :x")
	public List<Projet> findProjets(@Param("x")Long id_service);*/

}
