package ma.ensa.gi.ssm.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.ensa.gi.ssm.model.Entityservice;
import ma.ensa.gi.ssm.model.Personnel;

public interface PersonnelDao extends JpaRepository<Personnel, Long> {

	//@Query("select p from Personnel p where p.prenom like :x")
	//public Personnel findByNom(String nom);
	public Personnel findByNom(String nom);
	public Personnel findByService(Entityservice service);
	
	/*@Query("select p from Personnel p where p.id_service like :y")
	public Optional<Personnel> findByIdService(@Param("y")Long id_service);*/
}
