package ma.ensa.gi.ssm.dao;

import ma.ensa.gi.ssm.model.Commentaire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentaireDao extends JpaRepository<Commentaire, Long> {

}
