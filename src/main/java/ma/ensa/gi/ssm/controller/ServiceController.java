package ma.ensa.gi.ssm.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.ensa.gi.ssm.dao.ServiceDao;
import ma.ensa.gi.ssm.model.Entityservice;

@RestController
@RequestMapping("/api")
public class ServiceController {
	
	@Autowired
	private ServiceDao servicedao;
	
	/*@Autowired
	private ServiceService serviceS;*/

	public ServiceController(ServiceDao servicedao) {
		super();
		this.servicedao = servicedao;
	}
	
	@PostMapping("/service")
	ResponseEntity<Entityservice> createService(@RequestBody Entityservice service) throws URISyntaxException{
	  Entityservice result= servicedao.save(service);
	  return ResponseEntity.created(new URI("/api/service" + result.getId_service())).body(result); 
	}
	
	  
	
	@GetMapping("/services")
	Collection<Entityservice> services(){
		return servicedao.findAll();
	}
	
	/*@GetMapping("service/personnel/{id}")
	ResponseEntity<?> getPersonnelOfService(@PathVariable Long id){
		Optional<Personnel> personnel = personneldao.findByIdService(id);
		 return personnel.map(response -> ResponseEntity.ok().body(response))
				 .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
		 
		}*/
	/*@GetMapping("/service/{id}/projets")
	Collection<Service> servicesProjet(@PathVariable Long id){
		Optional<Service> service = servicedao.findById(id);
		return service.getProjet.;
	}*/
	
	@GetMapping("/service/{id}")
	ResponseEntity<?> getService(@PathVariable Long id){
	Optional<Entityservice> service = servicedao.findById(id);
	 return service.map(response -> ResponseEntity.ok().body(response))
			 .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@PutMapping("/service/{id}")
	ResponseEntity<Entityservice> updateService(@Valid @RequestBody Entityservice service){
		Entityservice result= servicedao.save(service);
		return ResponseEntity.ok().body(result);
	}
	
	@DeleteMapping("/service/{id}")
	ResponseEntity<?> deleteService(@PathVariable Long id){
		servicedao.deleteById(id);
		return ResponseEntity.ok().build();
	}
}
