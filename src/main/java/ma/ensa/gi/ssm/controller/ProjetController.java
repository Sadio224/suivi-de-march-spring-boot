package ma.ensa.gi.ssm.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ma.ensa.gi.ssm.dao.ProjetDao;
import ma.ensa.gi.ssm.model.Projet;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class ProjetController {
	
	@Autowired
	ProjetDao projetdao;

	public ProjetController(ProjetDao projetdao) {
		super();
		this.projetdao = projetdao;
	}
	
	@GetMapping("/projets")
	Collection<Projet> projets(){
		return projetdao.findAll();
	}

	@PostMapping("/projet")
	ResponseEntity<Projet> createProjet(@RequestBody Projet projet) throws URISyntaxException {
		Projet result= projetdao.save(projet);
		return ResponseEntity.created(new URI("/api/projet" + result.getId_projet())).body(result);
	}

	@GetMapping("/projet/{id}")
	ResponseEntity<?> getProjet(@PathVariable Long id){
		Optional<Projet> projet = projetdao.findById(id);
		return projet.map(response -> ResponseEntity.ok().body(response))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@PutMapping("/projet/{id}")
	ResponseEntity<Projet> updateProjet(@Valid @RequestBody Projet projet){
		Projet result= projetdao.save(projet);
		return ResponseEntity.ok().body(result);
	}

	@PatchMapping("/projet/{id}")
	ResponseEntity<Projet> updatePatchProjet(@Valid @RequestBody Projet projet){
		Projet result= projetdao.save(projet);
		return ResponseEntity.ok().body(result);
	}

	@DeleteMapping("/projet/{id}")
	ResponseEntity<?> deleteProjet(@PathVariable Long id){
		projetdao.deleteById(id);
		return ResponseEntity.ok().build();
	}

}
