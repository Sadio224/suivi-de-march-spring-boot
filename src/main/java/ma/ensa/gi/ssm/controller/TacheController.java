package ma.ensa.gi.ssm.controller;

import ma.ensa.gi.ssm.dao.TacheDao;
import ma.ensa.gi.ssm.model.Tache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class TacheController {

    @Autowired
    TacheDao tachedao;

    public TacheController(TacheDao tachedao) {
        super();
        this.tachedao = tachedao;
    }

    @GetMapping("/taches")
    Collection<Tache> taches(){
        return tachedao.findAll();
    }

    @PostMapping("/tache")
    ResponseEntity<Tache> createTache(@RequestBody Tache tache) throws URISyntaxException {
        Tache result= tachedao.save(tache);
        return ResponseEntity.created(new URI("/api/tache" + result.getId_tache())).body(result);
    }

    @GetMapping("/tache/{id}")
    ResponseEntity<?> getTache(@PathVariable Long id){
        Optional<Tache> tache = tachedao.findById(id);
        return tache.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/tache/{id}")
    ResponseEntity<Tache> updateTache(@Valid @RequestBody Tache tache){
        Tache result= tachedao.save(tache);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/tache/{id}")
    ResponseEntity<?> deleteTache(@PathVariable Long id){
        tachedao.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
