package ma.ensa.gi.ssm.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ma.ensa.gi.ssm.dao.PersonnelDao;
import ma.ensa.gi.ssm.model.Personnel;


@RestController
@RequestMapping("/api")
public class PersonnelController {
	
	@Autowired
	PersonnelDao personneldao;


	public PersonnelController(PersonnelDao personneldao) {
		super();
		this.personneldao = personneldao;
	}
	
	@GetMapping("/personnels")
	Collection<Personnel> personnels(){
		return personneldao.findAll();
	}
	
	@GetMapping("/personnel/{id}")
	ResponseEntity<Personnel> getPersonnel(@PathVariable Long id){
	Optional<Personnel> personnel = personneldao.findById(id);
	 return personnel.map(response -> ResponseEntity.ok().body(response))
			 .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@GetMapping("/personnel/nom/{nom}")
	@ResponseBody
	public Personnel getByNom(@PathVariable String nom) {
		Long personneId;
		Personnel perso = personneldao.findByNom(nom);
			personneId = Long.valueOf(perso.getId_personnel().toString());
		
			return perso;
	}
	
	@PostMapping("/personnel")
	ResponseEntity<Personnel> createPersonnel(@RequestBody Personnel personnel) throws URISyntaxException{
		Personnel result= personneldao.save(personnel);
	  return ResponseEntity.created(new URI("/api/personnel" + result.getId_personnel())).body(result); 
	}
	
	@PutMapping("/personnel/{id}")
	ResponseEntity<Personnel> updatePersonnel(@Valid @RequestBody Personnel personnel){
		Personnel result= personneldao.save(personnel);
		return ResponseEntity.ok().body(result);
	}
	
	@DeleteMapping("/personnel/{id}")
	ResponseEntity<?> deletePersonnel(@PathVariable Long id){
		personneldao.deleteById(id);
		return ResponseEntity.ok().build();
	}
}
