package ma.ensa.gi.ssm.controller;

import ma.ensa.gi.ssm.dao.CommentaireDao;
import ma.ensa.gi.ssm.model.Commentaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CommentaireController {

    @Autowired
    CommentaireDao commentairedao;

    @GetMapping("/comments")
    Collection<Commentaire> commentaires(){
        return commentairedao.findAll();
    }

    @GetMapping("/comment/{id}")
    ResponseEntity<?> getComment(@PathVariable Long id){
        Optional<Commentaire> comment = commentairedao.findById(id);
        return comment.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/comment")
    ResponseEntity<Commentaire> createComment(@RequestBody Commentaire comment) throws URISyntaxException {
        Commentaire result= commentairedao.save(comment);
        return ResponseEntity.created(new URI("/api/comment" + result.getId_commentaire())).body(result);
    }

    @PutMapping("/comment/{id}")
    ResponseEntity<Commentaire> updateComment(@Valid @RequestBody Commentaire comment){
        Commentaire result= commentairedao.save(comment);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/comment/{id}")
    ResponseEntity<?> deleteComment(@PathVariable Long id){
        commentairedao.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
