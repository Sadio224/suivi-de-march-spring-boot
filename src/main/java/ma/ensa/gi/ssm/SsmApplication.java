package ma.ensa.gi.ssm;

import java.net.URISyntaxException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsmApplication {

	public static void main(String[] args) throws URISyntaxException {
		SpringApplication.run(SsmApplication.class, args);
	}

}